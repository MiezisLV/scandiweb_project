<?php
include_once('./src/connection.php');
include_once('./src/select_scandiweb.php');
include_once('./src/select_product_list.php');
// if there is some problem, mysql will report it
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="./style/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>
<body>

<header>
    <h3 id="heading">Product list</h3>
    <form action="product_add.php">
        <input class="addbutt" type="submit" value="Add products">

        <!-- Display delete status -->
        <?php if (!empty($deleteMessage)) { ?>
            <div class="alert alert-success"><?php echo $deleteMessage; ?></div>
        <?php } ?>
    </form>
    <form action="product_list.php">
        <button type="button" method="post" id="btn_delete">Delete records</button>
</header>
<hr>

<!-- Dispay all database datas -->
<?php foreach ($scandiweb as $scandiweb) { ?>

    <div class="box">
        <div>
            <input id="checkbox" type="checkbox" name="SKU_id[]" value='<?php echo $scandiweb['SKU']; ?>'>
            <br>
            <?php echo htmlspecialchars($scandiweb['SKU']); ?>
            <br>
            <?php echo htmlspecialchars($scandiweb['Name']); ?>
            <br>
            <?php echo htmlspecialchars($scandiweb['Price']); ?><?php echo "$" ?>
            <br>
            <?php echo htmlspecialchars($scandiweb['Type_switcher']); ?>
            <br>
            <!-- Missing code -->


        </div>
    </div>
<?php } ?>
</form>
<!-- Delete script -->
<?php
include_once('delete_script.php')
?>
</body>
</html>
