<?php
include_once('./src/connection.php');
// make sql
$sql = 'SELECT SKU, Name, Price, Type_switcher FROM scandiweb';

// get the query result
$result = mysqli_query($conn, $sql);

// fetch the result in array format
$scandiweb = mysqli_fetch_all($result, MYSQLI_ASSOC);

// free result from memory
mysqli_free_result($result);

?>