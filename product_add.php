<?php
include_once('./src/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Document</title>
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
<!-- Header -->
<header>
    <h3>Product add</h3>
    <form action="product_list.php">
        <input type="submit" value="To product list">
    </form>
</header>
<hr>

<!--  -->

<!-- Form -->
<form id="inserts" class="list" action="./src/insert.php" method="POST">
    <div class="label">
        <label for="sku">SKU</label>
        <input placeholder="SKU is auto generated" type="text" id="sku" name="sku" disabled/>
        <br/>
    </div>
    <div class="label">
        <label for="name">Name of product:</label>
        <br>
        <input type="text" id="name" name="name" required/>
        <br/>
    </div>
    <div class="label">
        <label for="price">Price</label>
        <br>
        <input type="text" id="price" name="price" required/>
        <br/>

        <!-- Checkboxes -->
        <div>
            <input type="checkbox" id="size" name="switcher" value="DVD">
            <label for="switcher">You want to add <b>DVD</b>?</label>
        </div>

        <div>
            <input type="checkbox" id="dimensions" name="switcher" value="Furniture">
            <label for="switcher">You want to add a <b>furniture</b>?</label>
        </div>

        <div>
            <input type="checkbox" id="weight" name="switcher" value="Book">
            <label for="switcher">You want to add a <b>book</b>?</label>
        </div>

        <!--  -->

        <!-- Message -->
        <div id="show" style="display:none">
            <label>Add DVD size</label>
            <br>
            <label>Size</label>
            <input name="dvd" type="text"/>
            <br>
            <p>Please use MB format</p>
        </div>

        <div id="show2" style="display:none">
            <label for="message2">Add size for this furniture</label>
            <br>
            <label>Height</label>
            <input name="furn" type="text"/>
            <br>
            <label>Width</label>
            <input name="furn2" type="text">
            <br>
            <label>Lenght</label>
            <input name="furn3" type="text">
            <br>
            <p>Please use HxWxL format for furniture</p>

        </div>


        <div id="show3" style="display:none">
            <label for="book_message">Add books size</label>
            <br>
            <label>Weight</label>
            <input name="book" type="text">
            <br>
            <p>Please use KG format</p>

        </div>
        <br/>
        <button value="submit">Submit</button>
        <!--  -->
</form>

<?php
include_once('./src/toggle_script.js')
?>

</body>
</html>
