<?php
include_once 'connectionClass.class.php';

class Test extends dataB
{

    // Function to echo data from data base
    public function getUser()
    {
        $sql = "SELECT * FROM scandiweb";
        $stmt = $this->connect()->query($sql);
        while ($row = $stmt->fetch()) {
            echo $row['SKU'] . '<br>';
        }

    }

    // Function to get user input
    public function getUserInput($SKU)
    {
        $sql = "SELECT * FROM scandiweb WHERE SKU = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$SKU]);
        $data = $stmt->fetchAll();

        foreach ($data as $data) {
            echo $data['SKU'] . '<br>';
        }

    }
}

?>