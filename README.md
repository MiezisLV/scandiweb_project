## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is my second serious project, a lot of things still need to be changed and have not been completed, but the main function of the project is fulfilling.

* Things to fix in the future - echo connection of the second table (problems with MYSQL database)

* Complete the project with classes and MVC layout.

* Fix PHP PDO
	
## Technologies
Project is created with:
* MySQL
* PHP 
* HTML
* CSS/SASS
* JS
* VS

	
## Setup
To run this project, install it locally, install XAMPP and start Apache, and Mysql module, open http://localhost/phpmyadmin and import created tables, they are in zip file, open an online browser, type- http://localhost/scandiweb/product_list.php
and have fun.

```
http://localhost/scandiweb/product_list.php?

http://localhost/scandiweb/product_add.php?

```
## Contact information
| Made by            |
| -------------      |
| Andris Miezis      |
| amiezis36@gmail.com|
| +371 20235089      |
